package com.example.downloader.listener;

import com.example.downloader.model.VideoTaskItem;

import java.util.List;

public interface IDownloadInfosCallback {
    void onDownloadInfos(List<VideoTaskItem> items);
}
