package com.example.downloader.model;

import com.example.downloader.m3u8.M3U8;
import com.example.downloader.utils.Utility;

public class VideoTaskItem implements Cloneable {

    private final String mUrl;           //Unduh url video
    private long mDownloadCreateTime;    //Unduh waktu pembuatan
    private int mTaskState;              //Status tugas saat ini
    private String mMimeType;            //Jenis mime url video
    private String mFinalUrl;            //URL setelah lompatan 30x
    private int mErrorCode;              //Kode kesalahan unduhan tugas saat ini
    private int mVideoType;              //Jenis file saat ini
    private M3U8 mM3U8;                  //Struktur M3U8, jika bukan M3U8, maka null
    private int mTotalTs;                //Total pecahan M3U8 saat ini
    private int mCurTs;                  //Pecahan cache M3U8 saat ini
    private float mSpeed;                //Kecepatan unduh saat ini, fungsi getSpeedString dapat memformat kecepatan
    private float mPercent;              //Persentase unduhan saat ini, 0 ~ 100, adalah angka floating point
    private long mDownloadSize;          //Ukuran yang diunduh, fungsi getDownloadSizeString dapat memformat ukuran
    private long mTotalSize;             //Ukuran file total, file M3U8 tidak dapat diketahui secara akurat
    private String mFileHash;            //Md5 dari nama file
    private String mSaveDir;             //Nama direktori file untuk menyimpan file video
    private boolean mIsCompleted;        //Apakah unduhan sudah selesai
    private boolean mIsInDatabase;       //Apakah akan menyimpan dalam database
    private long mLastUpdateTime;        //Waktu terakhir kali database diperbarui
    private String mFileName;            //nama file
    private String mFilePath;            //Jalur lengkap file (termasuk nama file)
    private boolean mPaused;

    public VideoTaskItem(String url) { mUrl = url; }

    public String getUrl() {
        return mUrl;
    }

    public void setDownloadCreateTime(long time) { mDownloadCreateTime = time; }

    public long getDownloadCreateTime() { return mDownloadCreateTime; }

    public void setTaskState(int state) { mTaskState = state; }

    public int getTaskState() { return mTaskState; }

    public void setMimeType(String mimeType) { mMimeType = mimeType; }

    public String getMimeType() {
        return mMimeType;
    }

    public void setFinalUrl(String finalUrl) { mFinalUrl = finalUrl; }

    public String getFinalUrl() { return mFinalUrl; }

    public void setErrorCode(int errorCode) { mErrorCode = errorCode; }

    public int getErrorCode() {
        return mErrorCode;
    }

    public void setVideoType(int type) { mVideoType = type; }

    public int getVideoType() {
        return mVideoType;
    }

    public void setM3U8(M3U8 m3u8) { mM3U8 = m3u8; }

    public M3U8 getM3U8() {
        return mM3U8;
    }

    public void setTotalTs(int count) { mTotalTs = count; }

    public int getTotalTs() { return mTotalTs; }

    public void setCurTs(int count) { mCurTs = count; }

    public int getCurTs() { return mCurTs; }

    public void setSpeed(float speed) {
        mSpeed = speed;
    }

    public float getSpeed() {
        return mSpeed;
    }

    public String getSpeedString() {
        return Utility.getSize((long)mSpeed) + "/s";
    }

    public void setPercent(float percent) {
        mPercent = percent;
    }

    public float getPercent() {
        return mPercent;
    }

    public String getPercentString() {
        return Utility.getPercent(mPercent);
    }

    public void setDownloadSize(long size) {
        mDownloadSize = size;
    }

    public long getDownloadSize() {
        return mDownloadSize;
    }

    public String getDownloadSizeString() {
        return Utility.getSize(mDownloadSize);
    }

    public void setTotalSize(long size) {
        mTotalSize = size;
    }

    public long getTotalSize() { return mTotalSize; }

    public void setFileHash(String md5) { mFileHash = md5; }

    public String getFileHash() { return mFileHash; }

    public void setSaveDir(String path) { mSaveDir = path; }

    public String getSaveDir() { return mSaveDir; }

    public void setIsCompleted(boolean completed) { mIsCompleted = completed; }

    public boolean isCompleted() { return mIsCompleted; }

    public void setIsInDatabase(boolean in) { mIsInDatabase = in; }

    public boolean isInDatabase() { return mIsInDatabase; }

    public void setLastUpdateTime(long time) { mLastUpdateTime = time; }

    public long getLastUpdateTime() { return mLastUpdateTime; }

    public void setFileName(String name) { mFileName = name; }

    public String getFileName() { return mFileName; }

    public void setFilePath(String path) { mFilePath = path; }

    public String getFilePath() { return mFilePath;}

    public void setPaused(boolean paused) { mPaused = paused; }

    public boolean isPaused() { return mPaused; }

    public boolean isRunningTask() {
        return mTaskState == VideoTaskState.DOWNLOADING;
    }

    public boolean isPendingTask() {
        return mTaskState == VideoTaskState.PENDING;
    }

    public boolean isErrorState() { return mTaskState == VideoTaskState.ERROR; }

    public boolean isSuccessState() {
        return mTaskState == VideoTaskState.SUCCESS;
    }

    public boolean isInterruptTask() {
        return mTaskState == VideoTaskState.PAUSE || mTaskState == VideoTaskState.ERROR;
    }

    public boolean isInitialTask() {
        return mTaskState == VideoTaskState.DEFAULT;
    }

    public boolean isNonHlsType() {
        return mVideoType == Video.Type.MKV_TYPE ||
                mVideoType == Video.Type.QUICKTIME_TYPE ||
                mVideoType == Video.Type.WEBM_TYPE ||
                mVideoType == Video.Type.GP3_TYPE ||
                mVideoType == Video.Type.MP4_TYPE;
    }

    public boolean isHlsType() {
        return mVideoType == Video.Type.HLS_TYPE;
    }

    public void reset() {
        mTaskState = VideoTaskState.DEFAULT;
        mDownloadCreateTime = 0L;
        mMimeType = null;
        mErrorCode = 0;
        mVideoType = Video.Type.DEFAULT;
        mTaskState = VideoTaskState.DEFAULT;
        mM3U8 = null;
        mSpeed = 0.0f;
        mPercent = 0.0f;
        mDownloadSize = 0;
        mTotalSize = 0;
        mFileName = null;
        mFilePath = null;
    }

    @Override
    public Object clone() {
        VideoTaskItem taskItem = new VideoTaskItem(mUrl);
        taskItem.setDownloadCreateTime(mDownloadCreateTime);
        taskItem.setTaskState(mTaskState);
        taskItem.setMimeType(mMimeType);
        taskItem.setErrorCode(mErrorCode);
        taskItem.setVideoType(mVideoType);
        taskItem.setPercent(mPercent);
        taskItem.setDownloadSize(mDownloadSize);
        taskItem.setTotalSize(mTotalSize);
        taskItem.setFileHash(mFileHash);
        taskItem.setFilePath(mFilePath);
        taskItem.setFileName(mFileName);
        return taskItem;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj != null && obj instanceof VideoTaskItem) {
            String objUrl = ((VideoTaskItem)obj).getUrl();
            return mUrl.equals(objUrl);
        }
        return false;
    }

    public String toString() {
        return "VideoTaskItem[Url="+mUrl+
                ",Type="+mVideoType+
                ",Percent="+mPercent+
                ",DownloadSize="+mDownloadSize+
                ",State="+mTaskState+
                ",FilePath="+mFileName+
                ",LocalFile="+mFilePath+"]";
    }
}

