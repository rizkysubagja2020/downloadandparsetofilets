package com.example.downloadandparsetofilets

import android.app.Application
import com.example.downloader.DownloadConstants
import com.example.downloader.VideoDownloadConfig
import com.example.downloader.VideoDownloadManager
import com.example.downloader.utils.VideoDownloadUtils
import java.io.File

class MyApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        val file: File = VideoDownloadUtils.getVideoCacheDir(this)
        if (!file.exists()) {
            file.mkdir()
        }
        val config: VideoDownloadConfig = VideoDownloadManager.Build(this)
            .setCacheRoot(file)
            .setUrlRedirect(true)
            .setTimeOut(DownloadConstants.READ_TIMEOUT, DownloadConstants.CONN_TIMEOUT)
            .setConcurrentCount(DownloadConstants.CONCURRENT)
            .setIgnoreCertErrors(true)
            .buildConfig()
        VideoDownloadManager.getInstance().initConfig(config)
    }
}