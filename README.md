# Manifest
````
<uses-permission android:name="android.permission.INTERNET" />
    <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />
    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
````

# 1. Create new Module Android (downloader)
(https://gitlab.com/rizkysubagja2020/downloadandparsetofilets/-/tree/master/downloader)

# 2. Implement modules inside build.gradle(.app)
````kotlin
dependencies {

    implementation "org.jetbrains.kotlin:kotlin-stdlib:$kotlin_version"
    implementation 'androidx.core:core-ktx:1.3.2'
    implementation 'androidx.appcompat:appcompat:1.2.0'
    implementation 'com.google.android.material:material:1.2.1'
    implementation 'androidx.constraintlayout:constraintlayout:2.0.4'
    testImplementation 'junit:junit:4.+'
    androidTestImplementation 'androidx.test.ext:junit:1.1.2'
    androidTestImplementation 'androidx.test.espresso:espresso-core:3.3.0'

    // Implement Costume Module
    implementation project(path: ':downloader')
}
````

# 3. in Android Manifest (downloader)
````kotlin
<?xml version="1.0" encoding="utf-8"?>
<manifest xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    package="com.example.downloader">

    <application
        android:usesCleartextTraffic="true"
        tools:targetApi="m" />

</manifest>
````

# 4. Cerate package database, listener, m3u8, model, task, utils.

## a. inside package database
#### [VideoDownloadDatabaseHelper.java](https://gitlab.com/rizkysubagja2020/downloadandparsetofilets/-/blob/master/downloader/src/main/java/com/example/downloader/database/VideoDownloadDatabaseHelper.java)
#### [VideoDownloadSQLiteHelper.java](https://gitlab.com/rizkysubagja2020/downloadandparsetofilets/-/blob/master/downloader/src/main/java/com/example/downloader/database/VideoDownloadSQLiteHelper.java)

## b. inside package listener
#### [DownloadListener.java](https://gitlab.com/rizkysubagja2020/downloadandparsetofilets/-/blob/master/downloader/src/main/java/com/example/downloader/listener/DownloadListener.java)
#### [IDownloadInfosCallback.java](https://gitlab.com/rizkysubagja2020/downloadandparsetofilets/-/blob/master/downloader/src/main/java/com/example/downloader/listener/IDownloadInfosCallback.java)
#### [IDownloadListener.java](https://gitlab.com/rizkysubagja2020/downloadandparsetofilets/-/blob/master/downloader/src/main/java/com/example/downloader/listener/IDownloadListener.java)
#### [IDownloadTaskListener.java](https://gitlab.com/rizkysubagja2020/downloadandparsetofilets/-/blob/master/downloader/src/main/java/com/example/downloader/listener/IDownloadTaskListener.java)
#### [IVideoInfoListener.java](https://gitlab.com/rizkysubagja2020/downloadandparsetofilets/-/blob/master/downloader/src/main/java/com/example/downloader/listener/IVideoInfoListener.java)
#### [IVideoInfoParseListener.java](https://gitlab.com/rizkysubagja2020/downloadandparsetofilets/-/blob/master/downloader/src/main/java/com/example/downloader/listener/IVideoInfoParseListener.java)

## c. inside package m3u8
#### [M3U8.java](https://gitlab.com/rizkysubagja2020/downloadandparsetofilets/-/blob/master/downloader/src/main/java/com/example/downloader/m3u8/M3U8.java)
#### [M3U8Ts](https://gitlab.com/rizkysubagja2020/downloadandparsetofilets/-/blob/master/downloader/src/main/java/com/example/downloader/m3u8/M3U8Ts.java)
#### [M3U8Utils](https://gitlab.com/rizkysubagja2020/downloadandparsetofilets/-/blob/master/downloader/src/main/java/com/example/downloader/m3u8/M3U8Utils.java)

## d. inside package model
#### [Video.java](https://gitlab.com/rizkysubagja2020/downloadandparsetofilets/-/blob/master/downloader/src/main/java/com/example/downloader/model/Video.java) 
#### [VideoTaskItem.java](https://gitlab.com/rizkysubagja2020/downloadandparsetofilets/-/blob/master/downloader/src/main/java/com/example/downloader/model/VideoTaskItem.java)
#### [VideoTaskState.java](https://gitlab.com/rizkysubagja2020/downloadandparsetofilets/-/blob/master/downloader/src/main/java/com/example/downloader/model/VideoTaskState.java)

## e. inside package task
#### [BaseVideoDownloadTask.java](https://gitlab.com/rizkysubagja2020/downloadandparsetofilets/-/blob/master/downloader/src/main/java/com/example/downloader/task/BaseVideoDownloadTask.java)
#### [M3U8VideoDownloadTask.java](https://gitlab.com/rizkysubagja2020/downloadandparsetofilets/-/blob/master/downloader/src/main/java/com/example/downloader/task/M3U8VideoDownloadTask.java)
#### [VideoDownloadTask.java](https://gitlab.com/rizkysubagja2020/downloadandparsetofilets/-/blob/master/downloader/src/main/java/com/example/downloader/task/VideoDownloadTask.java)

## f. inside package utils
#### [DownloadExceptionUtils.java](https://gitlab.com/rizkysubagja2020/downloadandparsetofilets/-/blob/master/downloader/src/main/java/com/example/downloader/utils/DownloadExceptionUtils.java)
#### [HttpUtils.java](https://gitlab.com/rizkysubagja2020/downloadandparsetofilets/-/blob/master/downloader/src/main/java/com/example/downloader/utils/HttpUtils.java)
#### [LogUtils.java](https://gitlab.com/rizkysubagja2020/downloadandparsetofilets/-/blob/master/downloader/src/main/java/com/example/downloader/utils/LogUtils.java)
#### [Utility.java](https://gitlab.com/rizkysubagja2020/downloadandparsetofilets/-/blob/master/downloader/src/main/java/com/example/downloader/utils/Utility.java)
#### [VideoDownloadUtils.java](https://gitlab.com/rizkysubagja2020/downloadandparsetofilets/-/blob/master/downloader/src/main/java/com/example/downloader/utils/VideoDownloadUtils.java)
#### [WorkerThreadHandler.java](https://gitlab.com/rizkysubagja2020/downloadandparsetofilets/-/blob/master/downloader/src/main/java/com/example/downloader/utils/WorkerThreadHandler.java)


# 5. Create file DownloadConstan.java, VideoDownloadConfig.java, VideoDownloadException.java, VideoDownloadManager.java, VideoDownloadQueue.java, VideoInfoParserManager.java

#### a. [DownloadConstan.java](https://gitlab.com/rizkysubagja2020/downloadandparsetofilets/-/blob/master/downloader/src/main/java/com/example/downloader/DownloadConstants.java)

#### b. [VideoDownloadConfig.java](https://gitlab.com/rizkysubagja2020/downloadandparsetofilets/-/blob/master/downloader/src/main/java/com/example/downloader/VideoDownloadConfig.java)

#### c. [VideoDownloadException.java](https://gitlab.com/rizkysubagja2020/downloadandparsetofilets/-/blob/master/downloader/src/main/java/com/example/downloader/VideoDownloadException.java)

#### d. [VideoDownloadManager.java](https://gitlab.com/rizkysubagja2020/downloadandparsetofilets/-/blob/master/downloader/src/main/java/com/example/downloader/VideoDownloadManager.java)

#### e. [VideoDownloadQueue.java](https://gitlab.com/rizkysubagja2020/downloadandparsetofilets/-/blob/master/downloader/src/main/java/com/example/downloader/VideoDownloadQueue.java)

#### f. [VideoInfoParserManager.java](https://gitlab.com/rizkysubagja2020/downloadandparsetofilets/-/blob/master/downloader/src/main/java/com/example/downloader/VideoInfoParserManager.java)

# 6. Show Notification
````kotlin
class GetDialog {
    private var inputDialog: Dialog? = null
    // set variable channel id and notification manager
    private val channelId: String = "Progress Notification"
    private lateinit var notificationManager: NotificationManagerCompat


    interface EditTextStringResult {
        fun onString(str1: String?)
        // add Name, episode, image

    }

    fun getEditTextDialog(context: Context, click: EditTextStringResult): Dialog {
        inputDialog = Dialog(context, R.style.dialog_style)
        // set notification Manager
        notificationManager = NotificationManagerCompat.from(context)
        val layout: LinearLayout
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        layout = inflater.inflate(R.layout.dialog_input_url, null) as LinearLayout
        val edt1 = layout.findViewById<EditText>(R.id.edt_url)
        layout.findViewById<View>(R.id.tv_download).setOnClickListener(View.OnClickListener {
            val progressMax = 100
            // define notification
            val notification =
                NotificationCompat.Builder(context, channelId)
                    .setSmallIcon(R.drawable.ic_vertical_align_bottom)
                    .setContentTitle("Down To Download")
                    .setContentText("Downloading")
                    .setPriority(NotificationCompat.PRIORITY_LOW)
                    .setOngoing(false)
                    .setOnlyAlertOnce(true)
                    // progress
                    .setProgress(progressMax, 0, true)
                    .setAutoCancel(true)

            //Initial Alert
            notificationManager.notify(1, notification.build())
            val str1 = edt1.text.toString().trim { it <= ' ' }
            if (TextUtils.isEmpty(str1)) {
                Toast.makeText(context, "Masukkan URL kosong", Toast.LENGTH_SHORT)
                return@OnClickListener
            }
            click.onString(str1)
            inputDialog!!.dismiss()
        })
        layout.findViewById<View>(R.id.tv_cancel).setOnClickListener { inputDialog!!.dismiss() }
        val window = inputDialog!!.window
        window!!.setContentView(layout)
        window.setGravity(Gravity.CENTER)
        window.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        window.setWindowAnimations(R.style.popup_alpha_style)
        return inputDialog as Dialog
    }
}
````






















